/**
 * Created by nazar on 4/17/16.
 */
angular.module('app')
    .directive('selecNavbar', function(){
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'js/directive/template/selecNavbar.html',
            controller: function($scope, $location){

                $scope.manuItems = [
                    {title: 'Вступні випробування', href: '#!/vstup'},
                    {title: 'Правила прийому', href: 'files/pravyla_pruyomu_2014.pdf'},
                    {title: 'Договір про навчання (зразок)', href: 'files/dogovir.pdf'},
                    {title: 'Контакти приймальної комісії', href: '#!/kontpruym'},
                ];

            }
        }
    });