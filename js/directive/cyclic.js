/**
 * Created by nazar on 4/10/16.
 */
angular.module('app')
    .directive('cyclicNavbar', function(){
        return {
            replace: true,
            restrict: 'E',
            templateUrl: 'js/directive/template/cyclicNavbar.html',
            controller: function($scope, $location){

                $scope.manuItems = [
                    {title: 'Природничо-наукова підготовка', href: '#!/prurpidg'},
                    {title: 'Гуманітарна та соціально-економічна підготовка', href: '#!/gumpidg'},
                    {title: 'Інформатика та обчислювальна техніка', href: '#!/informatpidg'},
                    {title: 'Туристичне обслуговування', href: '#!/tourismpidg'},
                    {title: 'Фінанси і кредит', href: '#!/finansupidg'},
                    {title: 'Маркетингова діяльність', href: '#!/marketingpidg'}
                ];

            }
        }
    });