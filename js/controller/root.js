/**
 * Created by nazar on 4/10/16.
 */
angular.module('app')
    .controller("rootCtrl", function($rootScope, $location) {
        $rootScope.isActive = function (urlNow) {
            return urlNow == "#!" + $location.url();
        }
    })