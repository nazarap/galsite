/**
 * Created by nazar on 4/30/16.
 */
var app = angular.module("app");

app.controller("galaryCtrl", function($rootScope, $scope, $location, $http, $uibModal){

    $scope.allPhotos = [
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/000.jpg', description: "1", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/008.jpg', description: "2", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/009.jpg', description: "3", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/011.jpg', description: "4", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/012.jpg', description: "5", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/013.jpg', description: "6", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/014.jpg', description: "asd7asdasd", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/015.jpg', description: "asdasdasd", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/017.jpg', description: "asdasdasd", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/018.jpg', description: "asdasdasd", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/021.jpg', description: "asdasdasd", id: 1},
        {src: 'http://cbi.if.ua/jquery-lightbox/jquery-lightbox/images/bg/022.jpg', description: "asdasdasd", id: 1},
    ];

    $scope.addNewPhoto = function(){
        openModal();
    };

    $scope.openPhoto = function (photo) {
        openModal(photo);
    };

    var openModal = function(photo) {
        var modalInstance = $uibModal.open({
            templateUrl: 'template/openPhotoModal.html',
            controller: 'photoModalCtrl',
            resolve: {
                photo: function () {
                    return photo;
                }
            }
        });

        modalInstance.result.then(function (status) {
            if (status) {
                //getAllNews();
            }
        }, function () {
        });
    };

    $scope.deletePhoto = function() {

    }
});