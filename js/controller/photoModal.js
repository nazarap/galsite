/**
 * Created by nazar on 4/30/16.
 */
angular.module('app')
    .controller("photoModalCtrl", function($scope, $uibModalInstance, photo){
        $scope.title = photo ? "Перегляд фото" : "Додавання фото";
        $scope.photo = photo || {};

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });