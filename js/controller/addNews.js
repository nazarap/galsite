/**
 * Created by nazar on 4/30/16.
 */
angular.module('app').controller('addNewsCtrl', function ($scope, $uibModalInstance, news, $http) {

    $scope.title = news ? "Редагувати новину" : "Створити новину";
    $scope.news = news || {};

    $scope.ok = function () {

        var url;
        if($scope.news.id){
            url = '/update-news';
        } else {
            url = '/create-news';
        }
        $http.post(url, $scope.news).then(
            function(data){
                $uibModalInstance.close(true);
            }, function(err){
                $uibModalInstance.close(false);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});