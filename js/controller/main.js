/**
 * Created by nazar on 2/27/16.
 */
angular.module('app')
    .controller("mainCtrl", function($scope, $uibModal, $http){
        $scope.countClick = 0;
        $scope.openMyPage = false;
        $scope.step = 0;


        var getAllNews = function(){
            $http.get("news").then(
                function(data) {
                    $scope.allNews = data;
                },
                function(err){
                //test
                $scope.allNews = [{title: "new news",
                    id: 1,
                    description: "fgshajsakdasdasdljkjkjkasdjkljaklsdjklasdjkljklasdjkl dasljkdjlkasdjkl ajlskd jlaskjld " +
                    "jklasjkld ajklsdjkl ajksld jklasjkld jklasjkld asdasdkasd asjkdjkasd asdjkasd asdkj asdkjas da sdjkas dasd"}];
                }
            );
        };
        getAllNews();

        $scope.setCount = function() {
            ++$scope.countClick;
        };

        $scope.saveName = function() {
            $scope.isName = true;
        };

        $scope.addNews = function() {
            openModal();
        };

        $scope.changeNews = function(index) {
            openModal($scope.allNews[index]);
        };

        var openModal = function (news) {

            var modalInstance = $uibModal.open({
                templateUrl: 'template/addNews.html',
                controller: 'addNewsCtrl',
                resolve: {
                    news: function(){
                        return news;
                    }
                }
            });

            modalInstance.result.then(function (status) {
                if(status){
                    getAllNews();
                }
            }, function () {});
        };

        $scope.deleteNews = function(index) {
            $http.post("news", {id: $scope.allNews[index].id}).then(
                function(data){
                    $scope.allNews = data;
                },
                function(err){}
            );
        };

        $scope.next = function(step) {
            $scope.step = step;
            //switch (step){
            //    case '1':
            //
            //}
        }

        $scope.$watch("countClick", function(){
            if($scope.countClick == 5){
                $scope.openMyPage = true;
            }
        });
    });