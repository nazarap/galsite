/**
 * Created by nazar on 2/27/16.
 */
/**
 * Created by nazar on 30.10.15.
 */
var app = angular.module("app", ["ngRoute", "ui.bootstrap"]);

app.constant("URLS", {
    "MAIN": "/main",

    "ABOUT": "/about",

    "CONTACT": "/contact",

    "ACCOUNT": "/account",

    "LOGIN": "/login",

    "JOIN": "/join",

    "NEW": "/new",

    "ADMIN": "/admin",

    "SPEC1": "/spec1",
    "SPEC2": "/spec2",
    "SPEC3": "/spec3",
    "SPEC4": "/spec4",
    "SPEC5": "/spec5",

    "PRURPIDG": "/prurpidg",
    "GUMPING": "/gumpidg",
    "INFORMATPIDG": "/informatpidg",
    "TOURISMPIDG": "/tourismpidg",
    "FINANSUPIDG": "/finansupidg",
    "MARKETINGPIDG": "/marketingpidg",

    "VSTUP": "/vstup",
    "KONTPRUYM": "/kontpruym",

    "VR": "/vr",
    "VRCLUB": "/vrclub",
    "VRNAPR": "/vrnapr",

    "LIBRARY": "/library",

    "LOGIN": "/login",

    "GALARY": "/galary"
});

app.config(["$routeProvider", "URLS", "$locationProvider", function($routeProvider, URLS, $locationProvider) {

    $routeProvider
    .when(URLS.MAIN, {
        templateUrl: "/cbi.if.ua/template/main.html",
        controller: "mainCtrl"
    })
    .when(URLS.ABOUT, {
        templateUrl: "/cbi.if.ua/template/about.html",
        controller: "about"
    })
    .when(URLS.CONTACT, {
        templateUrl: "/cbi.if.ua/template/contact.html",
        controller: "contact"
    })
    .when(URLS.SPEC1, {
        templateUrl: "/cbi.if.ua/template/spec1.html",
        controller: "spec"
    })
    .when(URLS.SPEC2, {
        templateUrl: "/cbi.if.ua/template/spec2.html",
        controller: "spec"
    })
    .when(URLS.SPEC3, {
        templateUrl: "/cbi.if.ua/template/spec3.html",
        controller: "spec"
    })
    .when(URLS.SPEC4, {
        templateUrl: "/cbi.if.ua/template/spec4.html",
        controller: "spec"
    })
    .when(URLS.SPEC5, {
        templateUrl: "/cbi.if.ua/template/spec5.html",
        controller: "spec"
    })
        //
    .when(URLS.PRURPIDG, {
        templateUrl: "/cbi.if.ua/template/prurpidg.html",
        controller: "commissionCtrl"
    })
    .when(URLS.GUMPING, {
        templateUrl: "/cbi.if.ua/template/gumpidg.html",
        controller: "commissionCtrl"
    })
    .when(URLS.INFORMATPIDG, {
        templateUrl: "/cbi.if.ua/template/informatpidg.html",
        controller: "commissionCtrl"
    })
    .when(URLS.TOURISMPIDG, {
        templateUrl: "/cbi.if.ua/template/tourismpidg.html",
        controller: "commissionCtrl"
    })
    .when(URLS.FINANSUPIDG, {
        templateUrl: "/cbi.if.ua/template/finansupidg.html",
        controller: "commissionCtrl"
    })
    .when(URLS.MARKETINGPIDG, {
        templateUrl: "/cbi.if.ua/template/marketingpidg.html",
        controller: "commissionCtrl"
    })

        //
    .when(URLS.VSTUP, {
        templateUrl: "/cbi.if.ua/template/vstup.html",
        controller: "commissionCtrl"
    })

    .when(URLS.KONTPRUYM, {
        templateUrl: "/cbi.if.ua/template/kontpruym.html",
        controller: "commissionCtrl"
    })


    .when(URLS.VR, {
        templateUrl: "/cbi.if.ua/template/vr.html",
        controller: "vrCtrl"
    })

    .when(URLS.VRNAPR, {
        templateUrl: "/cbi.if.ua/template/vrnapr.html",
        controller: "vrCtrl"
    })

    .when(URLS.VRCLUB, {
        templateUrl: "/cbi.if.ua/template/vrclub.html",
        controller: "vrCtrl"
    })

    .when(URLS.LIBRARY, {
        templateUrl: "/cbi.if.ua/template/library.html",
        controller: "libraryCtrl"
    })

    .when(URLS.LOGIN, {
        templateUrl: "/cbi.if.ua/template/login.html",
        controller: "loginCtrl"
    })

    .when(URLS.GALARY, {
        templateUrl: "/cbi.if.ua/template/galary.html",
        controller: "galaryCtrl"
    })

    $routeProvider.otherwise({redirectTo: URLS.MAIN});


    $locationProvider.hashPrefix('!');
}]);